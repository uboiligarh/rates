package uz.devs.rate.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import uz.devs.rate.domain.helper.CurrencyType;
import uz.devs.rate.mapper.RateMapper;
import uz.devs.rate.model.response.ExchangeRateResponse;
import uz.devs.rate.repository.RateRepository;

import java.time.LocalDate;
import java.util.NoSuchElementException;

@RequiredArgsConstructor
@Configuration
public class DbInitializer implements ApplicationRunner {

    private final RateRepository rateRepository;
    private final RestTemplate restTemplate;

    private final RateMapper rateMapper = RateMapper.INSTANCE;

    @Override
    public void run(ApplicationArguments args) {
        if (rateRepository.count() == 0) {
            for (int i = 0; i <= 30; i++) {
                for (CurrencyType value : CurrencyType.values()) {

                    ExchangeRateResponse[] response =
                            restTemplate.getForObject(Constants.TARGET_URL + value.name() + "/" + LocalDate.now().minusDays(i) + "/",
                                    ExchangeRateResponse[].class);

                    if (response == null) {
                        throw new NoSuchElementException("Rate not found here!");
                    }

                    for (ExchangeRateResponse rate : response) {
                        rateRepository.save(rateMapper.toEntity(rate));
                    }

                }
            }
        }

    }

}
