package uz.devs.rate.scheduler;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import uz.devs.rate.domain.helper.CurrencyType;
import uz.devs.rate.mapper.RateMapper;
import uz.devs.rate.model.response.ExchangeRateResponse;
import uz.devs.rate.repository.RateRepository;
import uz.devs.rate.utils.Constants;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.NoSuchElementException;

@RequiredArgsConstructor
@Service
public class RateScheduler {
    private final RateRepository rateRepository;
    private final RestTemplate restTemplate;
    private final RateMapper rateMapper = RateMapper.INSTANCE;
    @Scheduled(fixedDelay = 5*60*1000, initialDelay = 60*1000)
    public void runEveryFiveMinutes() {
        LocalDate localDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String formattedString = localDate.format(formatter);
        if (!rateRepository.existsByDate(formattedString)) {

            for (CurrencyType value : CurrencyType.values()) {

                ExchangeRateResponse[] response =
                        restTemplate.getForObject(Constants.TARGET_URL + value.name() + "/" + LocalDate.now(),
                                ExchangeRateResponse[].class);

                if (response == null) {
                    throw new NoSuchElementException("Rate not found here!");
                }

                for (ExchangeRateResponse rate : response) {
                    rateRepository.save(rateMapper.toEntity(rate));
                }

            }
        }
    }
}
