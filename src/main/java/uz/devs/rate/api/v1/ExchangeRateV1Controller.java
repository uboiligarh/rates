package uz.devs.rate.api.v1;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import uz.devs.rate.service.RateService;
import uz.devs.rate.model.response.ExchangeRateResponse;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/rates/v1")
public class ExchangeRateV1Controller{
    private final RateService rateService;
    @GetMapping("/find")
    public List<ExchangeRateResponse> find(@RequestParam(name = "byName", required = false) String byName, @RequestParam(name="byDate", required = false) String date){
        List<ExchangeRateResponse> result = null;
        if (byName == null & date == null) {
            result = rateService.getAllRates();
        }
        return result;
    }

    public RateService getRateService() {
        return rateService;
    }
}
