package uz.devs.rate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.devs.rate.domain.RateEntity;

import java.util.UUID;
@Repository
public interface RateRepository extends JpaRepository<RateEntity, UUID> {
    Boolean existsByDate(String date);

}
