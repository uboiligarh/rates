package uz.devs.rate.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public record ExchangeRateResponse(
        @JsonProperty("id")
        Long id,
        @JsonProperty("Code")
        int code,
        @JsonProperty("Ccy")
        String currency,
        @JsonProperty("CcyNm_RU")
        String currencyNameRu,
        @JsonProperty("CcyNm_UZ")
        String currencyNameUz,
        @JsonProperty("CcyNm_UZC")
        String currencyNameUzC,
        @JsonProperty("CcyNm_EN")
        String currencyNameEn,
        @JsonProperty("Nominal")
        String nominal,
        @JsonProperty("Rate")
        double rate,
        @JsonProperty("Diff")
        double difference,
        @JsonProperty("Date")
        String date
) {

}
