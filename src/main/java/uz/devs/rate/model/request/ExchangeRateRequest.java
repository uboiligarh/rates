package uz.devs.rate.model.request;

import java.time.Instant;

public record ExchangeRateRequest(
        int code,
        String currency,
        String currencyNameRu,
        String currencyNameUz,
        String currencyNameUzC,
        String currencyNameEn,
        String nominal,
        double rate,
        double difference,
        Instant date

        ) {
}
