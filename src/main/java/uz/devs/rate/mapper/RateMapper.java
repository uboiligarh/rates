package uz.devs.rate.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import uz.devs.rate.domain.RateEntity;
import uz.devs.rate.model.response.ExchangeRateResponse;

import java.util.List;
import java.util.UUID;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface RateMapper {
    public static final RateMapper INSTANCE = Mappers.getMapper(RateMapper.class);

    @Mapping(target = "id", expression = "java(mapToRateResponseId(exchangeRateEntities))")
    public abstract List<ExchangeRateResponse> toResponse(List<RateEntity> rateEntity);

    default Long mapToRateResponseId(List<RateEntity> rateEntities) {
        return rateEntities.stream().map(RateEntity :: getId).flatMapToLong();
    }

}

