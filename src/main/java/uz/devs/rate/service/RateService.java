package uz.devs.rate.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.devs.rate.domain.RateEntity;
import uz.devs.rate.mapper.RateMapper;
import uz.devs.rate.model.response.ExchangeRateResponse;
import uz.devs.rate.repository.RateRepository;

import java.util.List;


@Service
@RequiredArgsConstructor
public class RateService {
        private final RateRepository rateRepository;
        private final RateMapper rateMapper = RateMapper.INSTANCE;

        public List<ExchangeRateResponse> getAllRates() {
                List<RateEntity> listEntity = rateRepository.findAll();
                return rateMapper.toResponse(listEntity);
        }
}
