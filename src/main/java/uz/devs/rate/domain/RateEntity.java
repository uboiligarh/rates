package uz.devs.rate.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import uz.devs.rate.domain.helper.CurrencyType;

import java.time.Instant;
import java.util.UUID;

@Setter
@Getter
@ToString
@Entity
@EntityListeners(AuditingEntityListener.class)
public class RateEntity {

        @Id
        @GeneratedValue(strategy = GenerationType.UUID)
        private UUID id;

        private int code;

        @Enumerated(value = EnumType.STRING)
        private CurrencyType currency;

        private String currencyNameRu;
        private String currencyNameUz;
        private String currencyNameUzC;
        private String currencyNameEn;
        private String nominal;
        private double rate;
        private double difference;

        private String date;

        @CreatedDate
        private Instant createdDate;

        @LastModifiedDate
        private Instant lastModifiedDate;
}
