package uz.devs.rate.domain.helper;

public enum CurrencyType {
    USD,
    EUR,
    RUB
}
